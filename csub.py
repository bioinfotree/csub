#!/usr/bin/env python
#
# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:
#	add support for ssh key (-i)
#	automatic load module from a list
#	load data from conf file
#	optimize dipendeces
#	allow memory suffixes to be M o G
#	parse Popen stdout
#	define queue

# Manuals:
# http://docs.adaptivecomputing.com/torque/4-1-3/Content/topics/commands/qsub.htm
# http://docs.adaptivecomputing.com/torque/4-1-3/Content/topics/commands/qsub.htm#extendedDesc

from argparse import ArgumentParser
from sys import stdin, stdout
from os	import mkdir, path, getcwd, environ
from re import sub
from subprocess import Popen, PIPE, STDOUT
from pprint import pprint



domain = 'appliedgenomics.org'
username = environ.get('USER')
hostname = environ.get('HOSTNAME')


def main():
	parser = ArgumentParser()
	parser.add_argument('--job-name', metavar='NAME', dest='job_name' ,default=username, help='use NAME as job name (default: %s)' %username)
	parser.add_argument('--mem', metavar='N', dest='memory' ,default='1300M', help='use N as memory limit (default: 1300M)')
	parser.add_argument('--nodes', metavar='N', dest='nodes' ,default=1, type=int, help='declare N>0 nodes (default: 1)')
	parser.add_argument('--procs-per-node', metavar='N', dest='procs_per_node', type=int, default=1, help='declare N>0 processors for each node (default: 1)')
	parser.add_argument('--nomail', dest='no_mail', action='store_true', help='do not send e-mail (default: send mail on errors and when job terminates)')
	parser.add_argument('--mail-on-error', dest='mail_on_error', action='store_true', help='send mail only on errors (default: send mail on errors and when job terminates)')
	parser.add_argument('--high-priority', dest='high_priority', action='store_true', help='set high priority for the job (default: normal priority)')
	parser.add_argument('--days', metavar='N', dest='days', default=0, type=int, help='declare maximum N days for the job to be in the running state')
	parser.add_argument('--after', metavar='JOB_ID', dest='after', nargs='+', help='this job is scheduled for execution after jobs JOB_ID have started execution')
	parser.add_argument('--afterok', metavar='JOB_ID', dest='afterok', nargs='+', help='this job is scheduled for execution only after jobs JOBS_ID have terminated with no errors')
	parser.add_argument('--afternotok', metavar='JOB_ID', dest='afternotok', nargs='+', help='this job is scheduled for execution only after jobs JOBS_ID have terminated with error')
	parser.add_argument('--afterany', metavar='JOB_ID', dest='afterany', nargs='+', help='this job is scheduled for execution after jobs JOBS_ID have terminated, with or without errors')
	parser.add_argument('--log-path', metavar='PATH', dest='log_path', default=getcwd(), help='where save logs (DEFAULT: %s)' %getcwd())
	parser.add_argument('--test', dest='test', action='store_true', help='do not send commands to qsub, only print to STDOUT')
	#parser.add_argument('--noenv', metavar='?', dest='noenv', help='do not load hard-coded env')
	
	args = parser.parse_args()

	# create new dir
	try:
	    mkdir(args.log_path)
	except OSError:
	    if path.exists(args.log_path):
	      pass
	    else:
	      raise # There was an error on creation, so make sure you know about it
	
	mail='ea'
	if args.no_mail:
	    mail='n'
	if args.mail_on_error:
	    mail='a'
	
	afterok=''
	if args.afterok:
	    afterok='afterok:%s' %(':'.join(args.afterok))
	
	afternotok=''
	if args.afternotok:
	    afternotok='afternotok:%s' %(':'.join(args.afternotok))
	
	afterany=''
	if args.afterany:
	    afterany='afterany:%s' %(':'.join(args.afterany))
	    
	after=''
	if args.after:
	    after='after:%s' %(':'.join(args.after))
	
	dependencies='depend=%s' %','.join(filter(None,(afterok,afternotok,afterany,after)))
	
	priority=0
	if args.high_priority:
	    priority=-50
	    
	hours=24
	if args.days:
	    hours= args.days*24
	
	cmd = ''
	for line in stdin:
	    token = line.strip().split()
	    cmd = cmd + ' '.join(token)
	    
	pbs_directive = '''
#PBS -N {job_name_option}
#PBS -d {workdir_option}
#PBS -j oe
#PBS -o {stdout_option}
#PBS -k n
#PBS -l nodes={nodes_option}:ppn={procs_option},vmem={mem_option},mem={mem_option}
#PBS -l walltime={hours_option}:00:00
#PBS -m {mail_option}
#PBS -M {user_list_option}
#PBS -p {priority_option}
#PBS -r n
#PBS -q {destination_option}
#PBS -V
#PBS -W {depend_option}
'''.format(job_name_option=args.job_name,
	   workdir_option=getcwd(),
	   nodes_option=args.nodes,
	   stdout_option=args.log_path,
	   procs_option=args.procs_per_node,
	   mem_option=sub(r'[gG]$','000M',args.memory,count=1),
	   hours_option=hours,
	   mail_option=mail,
	   user_list_option='%s@%s' %(username, domain),
	   priority_option=priority,
	   destination_option='batch',
	   real_command=cmd,
	   depend_option=dependencies)
 
	pbs_file = '''#!/bin/bash''' + \
	pbs_directive + \
'''
echo REMEMBER: OUTPUT is in file {stdout_option}/{job_name_option}.${{PBS_JOBID%%.*}}.log

##### LOG OUTPUT ####
(
cat <<EOF
====> Command(s) executed <====
'''.format(stdout_option=args.log_path, job_name_option=args.job_name) \
	   + pbs_directive + \
'''
cat <<EOF
(
echo -en
uname -a
{real_command}
) >{stdout_option}/{job_name_option}.${{PBS_JOBID%%.*}}.log 2>&1
====> Command(s) Output <====
EOF
##### INPUT COMMAND #####

{real_command}
) >>{stdout_option}/{job_name_option}.${{PBS_JOBID%%.*}}.log 2>&1
'''.format(stdout_option=args.log_path, job_name_option=args.job_name, real_command=cmd)
	
	if args.test:
	   stdout.write(pbs_file)
	else:
	   p1 = Popen(['ssh','stratocluster', 'qsub'],
		stdin=PIPE,
	        cwd=getcwd(), 
	        #stdout=PIPE,
	        #stderr=STDOUT,
	        shell=False)

	   # feed process stdin with string
	   p1_stdout, p1_stderr = p1.communicate(pbs_file)


if __name__ == '__main__':
	main()


